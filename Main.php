<?php
ini_set('display_errors',true);
error_reporting(E_ALL);

class Garden
{

    private $GardenStructure = array();
    //[type/id/]

    public $trees = NULL;

    public function __construct()
    {
        $this->GardenStructure['pears'] = array(
            'count' => 15,
            'minFruits' => 0,
            'maxFruits' => 20,
            'minWeight' => 130,
            'maxWeight' => 170,
        );
        $this->GardenStructure['apples'] = array(
            'count'=>10,
            'minFruits' => 40,
            'maxFruits' => 50,
            'minWeight' => 150,
            'maxWeight' => 180,
        );

        $this->initGarden();
    }


        private function getTreeWeight($count,$min,$max)
        {
            $weight = 0;
            for($i=1;$i<=$count;$i++):
                $weight = $weight+rand($min,$max);
            endfor;

            return $weight;
        }

        private function getFruitsFinalCount($min,$max)
        {
            return rand($min,$max);
        }

        private function initGarden()
        {
            foreach($this->GardenStructure as $treeName => $treeParams):
                for($i=1;$i<=$treeParams['count'];$i++):
                    $this->trees[$treeName][$treeName.'_'.$i]['fruits']['treesCount'] = $this->getFruitsFinalCount($treeParams['minFruits'],$treeParams['maxFruits']);
                    $this->trees[$treeName][$treeName.'_'.$i]['fruits']['treesFruitsWeight'] = $this->getTreeWeight($this->trees[$treeName][$treeName.'_'.$i]['fruits']['treesCount'],$treeParams['minWeight'],$treeParams['maxWeight']);
                endfor;
            endforeach;

            // dummy return
            return true;
        }



}


class gardenGrabber  extends Garden
{
    public $grabberHealth = 0;
    public function __construct()
    {
        parent::__construct();
    }

    public function initGrabber(int $grabberHealth=88)
    {

         //  let's say that our grabber has 88% health by the default
        $this->grabberHealth = $grabberHealth;

        return $this->getFruitsFinalData();

    }

    public function devide($num,$negative)
    {
        return round($num/$negative??null,1);
    }

    public function graberPercentageDevider(int $data)
    {

        return ($this->devide($data,1000)/100)*$this->grabberHealth;
    }


    public function getFruitsFinalData()
    {
        $treesFruits = [];

        $i=0;
        foreach($this->trees as $treeType=>$treeValues)
        {
            $treesFruits[$i]['name'] = $treeType;
            $treesFruits[$i]['totalCount'] = 0;
            $treesFruits[$i]['totalWeight'] = 0;
            foreach($treeValues as $treeID  => $fruits)
            {
                foreach($fruits as $singleTreeFruits)
                {
                    $treesFruits[$i]['totalCount'] = $treesFruits[$i]['totalCount']+$singleTreeFruits['treesCount'];
                    $treesFruits[$i]['totalWeight'] = $treesFruits[$i]['totalWeight']+$singleTreeFruits['treesFruitsWeight'];
                }
            }
            $i++;
        }

        return $treesFruits;
    }
}
    $gardenObject = new gardenGrabber();

    $garden = $gardenObject->initGrabber();


    echo 'We have generated '.count($garden).' types of the trees: <br />';

    // тут в echo в посередине поленился*1000)
    foreach($garden as $k=>$tree): echo '<strong>'.$tree['name']. '</strong> <i>('.$tree['totalCount'].' points with total weight '.$gardenObject->devide($tree["totalWeight"],1000).'kg). Grabbed: '.round($gardenObject->graberPercentageDevider($tree['totalCount']*1000),1).' points with total weight '.round($gardenObject->graberPercentageDevider($tree['totalWeight']),1).'</i> ';
    echo $k==array_key_first( $garden)?'<br /> and <br />':'';
    endforeach;

    echo '<p><span style="color:red;">Note: the grabber\'s healt stay\'s on '.$gardenObject->grabberHealth.'%</span></p>';




?>
